/**
 * This file defines a cell.
 *
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startCell(Actor cell) {
	setImageFile(cell, "cell.png");
}

/**
 * Act - do whatever the cell wants to do. This function
 * is called whenever the 'Act' or 'Run' button gets pressed
 * in the environment.
 */
void actCell(Actor cell) {

}

