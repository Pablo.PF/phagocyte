/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "cell.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_cell_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startCell(object);
}

JNIEXPORT void JNICALL Java_cell_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actCell(object);
}

