/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "phagocyte.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_phagocyte_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startPhagocyte(object);
}

JNIEXPORT void JNICALL Java_phagocyte_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actPhagocyte(object);
}

