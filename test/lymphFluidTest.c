/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "lymphFluid.c"

void testStartGame() {
    // Given
    World lymphFluid;

    // When
    lymphFluid = newWorld("lymphFluid");

    // Then
    assertEquals_int(560, getWidth(lymphFluid));
    assertEquals_int(560, getHeight(lymphFluid));
    assertEquals_int(  1, getCellSize(lymphFluid));
    assertEquals_int(  1, getActorsNumber(lymphFluid, "phagocyte"));
    assertEquals_int(  3, getActorsNumber(lymphFluid, "virus"));
    assertEquals_int( 10, getActorsNumber(lymphFluid, "cell"));
}

