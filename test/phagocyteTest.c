/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "phagocyte.c"

World lymphFluid;
Actor phagocyte;

void setUp() {
	// Given a lymph fluid with a phagocyte
	lymphFluid = newWorld("lymphFluid");
	int actorsNumber;
	Actor* actors = getActors(lymphFluid, "phagocyte", &actorsNumber);
	assertEquals_int(1, actorsNumber);
	phagocyte = actors[0];
}

void testGetX() {
	// Given
	setUp();

	// When
	int x = getX(phagocyte);

	// Then the phagocyte is still in the same place
	assertEquals_int(231, x);
}

void testGetY() {
	// Given
	setUp();

	// When
	int y = getY(phagocyte);

	// Then the phagocyte is still in the same place
	assertEquals_int(203, y);
}

void testGetRotation() {
	// Given
	setUp();

	// When nothing
	int rotation = getRotation(phagocyte);

	// Then the phagocyte is still in the same direction
	assertEquals_int(0, rotation);
}

void testGetImage() {
	// Given
	setUp();

	// When
	const char* imageFile = getImageFile(phagocyte);

	// Then the phagocyte has its initial image
	assertEquals_String("phagocyte.png", imageFile);
}

void testMove() {
	// Given the initial position
	setUp();
	int x = getX(phagocyte);
	int y = getY(phagocyte);

	// When any key is not typed
	actPhagocyte(phagocyte);

	// Then the phagocyte moves 5
	assertEquals_int(x+5, getX(phagocyte));
	assertEquals_int(  y, getY(phagocyte));
}

void testMoveBis() {
	// Given the initial position
	setUp();
	int x = getX(phagocyte);
	int y = getY(phagocyte);

	// When any key is not typed
	runOnceWorld(lymphFluid);

	// Then the phagocyte moves 5
	assertEquals_int(x+5, getX(phagocyte));
	assertEquals_int(  y, getY(phagocyte));
}

void testMoveBisBis() {
	// Given the initial position
	setUp();
	int x = getX(phagocyte);
	int y = getY(phagocyte);

	// When any key is not typed
	runOnceActors(&phagocyte,1);

	// Then the phagocyte moves 5
	assertEquals_int(x+5, getX(phagocyte));
	assertEquals_int(  y, getY(phagocyte));
}

void testTurnRight() {
	// Given the initial position
	setUp();
	assertEquals_int(0, getRotation(phagocyte));

	// When right key is typed
	typeKey("right");
	actPhagocyte(phagocyte);

	// Then the phagocyte rotates 4 degrees to the right
	assertEquals_int(4, getRotation(phagocyte));
}

void testTurnRightBis() {
	// Given the initial position
	setUp();
	int degrees = getRotation(phagocyte);

	// When right key is typed
	typeKey("right");
	actPhagocyte(phagocyte);

	// Then the phagocyte rotates 4 degrees to the right
	assertEquals_int((360+degrees+4)%360, getRotation(phagocyte));
}

void testTurnRightBisBis() {
	// Given the initial position
	setUp();
	setRotation(phagocyte, 10);

	// When right key is typed
	typeKey("right");
	actPhagocyte(phagocyte);

	// Then the phagocyte rotates 4 degrees to the right
	assertEquals_int(14, getRotation(phagocyte));
}

void testTurnLeft() {
	// Given the initial position
	setUp();
	assertEquals_int(0, getRotation(phagocyte));

	// When left key is typed
	typeKey("left");
	actPhagocyte(phagocyte);

	// Then the phagocyte rotates 4 degrees to the left
	assertEquals_int(356, getRotation(phagocyte));
}

void testTurnLeftBis() {
	// Given the initial position
	setUp();
	int degrees = getRotation(phagocyte);

	// When left key is typed
	typeKey("left");
	actPhagocyte(phagocyte);

	// Then the phagocyte rotates 4 degrees to the left
	assertEquals_int((360+degrees-4)%360, getRotation(phagocyte));
}

void testTurnLeftBisBis() {
	// Given the initial position
	setUp();
	setRotation(phagocyte, 10);

	// When left key is typed
	typeKey("left");
	actPhagocyte(phagocyte);

	// Then the phagocyte rotates 4 degrees to the left
	assertEquals_int(6, getRotation(phagocyte));
}

void testTurnRight90() {
	// Given a phagocyte that has rotated 90 degrees to the right
	setUp();
	turn(phagocyte, 90);

	// When any key is not typed
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte goes down 5
	assertEquals_int(  x, getX(phagocyte));
	assertEquals_int(y+5, getY(phagocyte));
}

void testTurnRight180() {
	// Given a phagocyte that has rotated 180 degrees to the right
	setUp();
	turn(phagocyte, 180);

	// When any key is not typed
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte goes back 5
	assertEquals_int(x-5, getX(phagocyte));
	assertEquals_int(  y, getY(phagocyte));
}

void testTurnRight270() {
	// Given a phagocyte that has rotated 270 degrees to the right
	setUp();
	turn(phagocyte, 270);

	// When any key is not typed
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte goes up 5
	assertEquals_int(  x, getX(phagocyte));
	assertEquals_int(y-5, getY(phagocyte));
}

void testTurnLeft90() {
	// Given a phagocyte that has rotated 90 degrees to the left
	setUp();
	turn(phagocyte, -90);

	// When any key is not typed
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte goes up 5
	assertEquals_int(  x, getX(phagocyte));
	assertEquals_int(y-5, getY(phagocyte));
}

void testTurnLeft180() {
	// Given a phagocyte that has rotated 180 degrees to the left
	setUp();
	turn(phagocyte, -180);

	// When any key is not typed
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte goes back 5
	assertEquals_int(x-5, getX(phagocyte));
	assertEquals_int(  y, getY(phagocyte));
}

void testTurnLeft270() {
	// Given a phagocyte that has rotated 270 degrees to the left
	setUp();
	turn(phagocyte, -270);

	// When any key is not typed
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte goes down 5
	assertEquals_int(  x, getX(phagocyte));
	assertEquals_int(y+5, getY(phagocyte));
}

void testLookForVirus() {
	// Given a virus in the same position as the phagocyte
	setUp();
	assertEquals_int(3, getActorsNumber(lymphFluid, "virus"));
	int actorsNumber;
	Actor virus = getActors(lymphFluid, "virus", &actorsNumber)[0];
	setLocation(virus, getX(phagocyte), getY(phagocyte));

	// When the phagocyte checks that there is a virus
	actPhagocyte(phagocyte);

	// Then the virus's complaint can be listened when the phagocyte eats it
	assertEquals_String("au.wav", getPlayedSound());
	assertEquals_int(2, getActorsNumber(lymphFluid, "virus"));
}

void testLookForVirusBis() {
	// Given a virus in the same position as the phagocyte
	setUp();
	Actor virus = newActor("virus");
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	addActorToWorld(lymphFluid, virus, x, y);

	// When the phagocyte checks that there is a virus
	actPhagocyte(phagocyte);

	// Then the virus's complaint can be listened when the phagocyte eats it
	assertEquals_String("au.wav", getPlayedSound());
	assertEquals_int(0, getActorsNumberAt(lymphFluid, x, y, "virus"));
}

void testChangeImage() {
	// Given a virus in the same position as the phagocyte
	setUp();
	Actor virus = newActor("virus");
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	addActorToWorld(lymphFluid, virus, x, y);

	// When the phagocyte checks that there is a virus
	actPhagocyte(phagocyte);

	// Then the phagocyte image is changed to phagocyte-1.png
	assertEquals_String("phagocyte-1.png", getImageFile(phagocyte));
}

void testChangeImageAgain() {
	// Given a virus in the same position as the phagocyte
	setUp();
	Actor virus = newActor("virus");
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	addActorToWorld(lymphFluid, virus, x, y);

	// When the phagocyte checks that there is a virus
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte image is changed to phagocyte-2.png after five acts
	assertEquals_String("phagocyte-2.png", getImageFile(phagocyte));
}

void testChangeImageAgainAgain() {
	// Given a virus in the same position as the phagocyte
	setUp();
	Actor virus = newActor("virus");
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	addActorToWorld(lymphFluid, virus, x, y);

	// When the phagocyte checks that there is a virus
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte image is changed to phagocyte-2.png after nine acts
	assertEquals_String("phagocyte-3.png", getImageFile(phagocyte));
}

void testChangeImageAgainAgainAgain() {
	// Given a virus in the same position as the phagocyte
	setUp();
	Actor virus = newActor("virus");
	int x = getX(phagocyte);
	int y = getY(phagocyte);
	addActorToWorld(lymphFluid, virus, x, y);

	// When the phagocyte checks that there is a virus
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);
	actPhagocyte(phagocyte);

	// Then the phagocyte image is changed to phagocyte-2.png after thirteen acts
	assertEquals_String("phagocyte.png", getImageFile(phagocyte));
}
