/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "cell.c"

World lymphFluid;
Actor cell;

void setUp() {
	// Given the first of the ten cells
	lymphFluid = newWorld("lymphFluid");
	int actorsNumber;
	assertEquals_int(10, getActorsNumber(lymphFluid, "cell"));
	cell = getActors(lymphFluid, "cell", &actorsNumber)[0];
}

void testNoMove() {
	// Given the initial position of the cell
	setUp();
	int x = getX(cell);
	int y = getY(cell);

	for (int k=0; k<20; k++) {
		// When time goes by
		actCell(cell);

		// Then the cell is still in the same place
		assertEquals_int(x, getX(cell));
		assertEquals_int(y, getY(cell));
	}
}
